package u04lab.code

import u04lab.code.Lists._
import u04lab.code.Optionals._
import u04lab.code.Streams.Stream
import u04lab.code.Streams.Stream._

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]

  def allSoFar(): List[A]

  def reversed(): PowerIterator[A]
}

object PowerIterator {
  def apply[A](stream: Stream[A]): PowerIterator[A] = PowerIteratorImpl(stream)

  private case class PowerIteratorImpl[A](var stream: Stream[A]) extends PowerIterator[A] {
    private var pastList: List[A] = List.Nil()

    override def next(): Option[A] = stream match {
      case Cons(head, tail) =>
        stream = tail()
        pastList = List.Cons(head(), pastList)
        Option.of(head())
      case _ => Option.empty
    }

    override def allSoFar(): List[A] = List.reverse(this.pastList)

    override def reversed(): PowerIterator[A] = PowerIteratorImpl(List.toStream(this.pastList))
  }

}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]

  def fromList[A](list: List[A]): PowerIterator[A]

  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIterator(Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = PowerIterator(List.toStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIterator(Stream.take(Stream.generate(Random.nextBoolean))(size))
}
