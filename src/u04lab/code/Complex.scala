package u04lab.code

trait Complex {
  def re: Double

  def im: Double

  def +(c: Complex): Complex // should implement the sum of two complex numbers..
  def *(c: Complex): Complex // should implement the product of two complex numbers
}


object Complex {
  def apply(re: Double, im: Double): Complex = ComplexImpl(re, im) // Fill here
  private case class ComplexImpl(override val re: Double, override val im: Double) extends Complex {
    override def +(c: Complex): Complex = ComplexImpl(this.re + c.re, c.im + this.im)

    override def *(c: Complex): Complex = ComplexImpl(this.re * c.re - this.im * c.im, this.re * c.im + this.im * c.re)

  }

}
