package u04lab.code

import u04lab.code.Lists.List._
import u04lab.code.Lists._ // import custom List type (not the one in Scala stdlib)

trait Student {
  def name: String

  def year: Int

  def enrolling(courses: Course*): Unit // the student participates to a Course
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
}

trait Course {
  def name: String
  def teacher: String
}

object Student {

  private class StudentImpl(val name: String, val year: Int) extends Student {
    private var courseList: List[Course] = List.Nil()

    override def enrolling(courses: Course*): Unit = for (course <- courses) this.courseList = List.Cons(course, this.courseList)

    override def courses: List[String] = List.map(this.courseList)(_.name)

    override def hasTeacher(teacher: String): Boolean = List.contains(List.map(this.courseList)(_.teacher))(teacher)
  }

  def apply(name: String, year: Int = 2017): Student = new StudentImpl(name, year)
}

object Course {

  private case class CourseImpl(name: String, teacher: String) extends Course

  def apply(name: String, teacher: String): Course = CourseImpl(name, teacher)
}

object sameTeacher {
  def unapply(courseList: List[Course]): Option[String] = courseList match {
    case Cons(h, Nil()) => Some(h.teacher)
    case Cons(h, sameTeacher(t)) if t == h.teacher => Some(t)
    case _ => Option.empty
  }
}
