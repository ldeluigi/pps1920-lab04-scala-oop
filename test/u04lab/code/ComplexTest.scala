package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ComplexTest {

  private val a = Array(Complex(10, 20), Complex(1, 1), Complex(7, 0))
  private val c = a(0) + a(1) + a(2)
  private val c2 = a(0) * a(1)

  @Test def testSumOp(): Unit = {
    assertEquals(18.0, c.re)
    assertEquals(21.0, c.im)
  }

  @Test def testMulOp(): Unit = {
    assertEquals(-10.0, c2.re)
    assertEquals(30.0, c2.im)
  }

  @Test def testApply() {
    assertEquals(Complex(18.0, 21.0), c)
    assertEquals(Complex(-10.0, 30.0), c2)
  }

  @Test def testEquals(): Unit = {
    assertTrue(c == Complex(c.re, c.im))
  }
}
