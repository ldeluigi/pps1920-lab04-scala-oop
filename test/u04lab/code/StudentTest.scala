package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.TestInstance.Lifecycle
import org.junit.jupiter.api.{BeforeAll, Test, TestInstance}
import u04lab.code.Lists.List
import u04lab.code.Lists.List._

@TestInstance(Lifecycle.PER_CLASS)
class StudentTest {
  private val cPPS = Course("PPS", "Viroli")
  private val cPCD = Course("PCD", "Ricci")
  private val cSDR = Course("SDR", "D'Angelo")
  private val cOOP = Course("OOP", "Viroli")
  private val s1 = Student("mario", 2015)
  private val s2 = Student("gino", 2016)
  private val s3 = Student("rino") //defaults to 2017

  @BeforeAll def beforeAll(): Unit = {
    s1.enrolling(cPPS)
    s1.enrolling(cPCD)
    s2.enrolling(cPPS)
    s3.enrolling(cPPS)
    s3.enrolling(cPCD)
    s3.enrolling(cSDR)
  }

  @Test def testHasTeacher(): Unit = {
    assertTrue(s1.hasTeacher("Ricci"))
    assertFalse(s1.hasTeacher("____"))
  }

  @Test def testEnrolledCourses(): Unit = {
    assertEquals(Cons("PCD", Cons("PPS", Nil())), s1.courses)
    assertEquals(Cons("PPS", Nil()), s2.courses)
    assertEquals(Cons("SDR", Cons("PCD", Cons("PPS", Nil()))), s3.courses)
  }

  @Test def testSameTeacher(): Unit = {
    assertFalse(List(cPPS, cPCD) match {
      case sameTeacher(t) => true
      case _ => false
    })
    assertTrue(List(cPPS, cOOP) match {
      case sameTeacher(t) => true
      case _ => false
    })
    assertEquals("Viroli", List(cPPS, cOOP) match {
      case sameTeacher(t) => t
      case _ => ""
    })
  }
}
